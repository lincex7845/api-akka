package co.s4n.akka.http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.util.Timeout
import co.s4n.akka.http.route.CrudResouces
import com.typesafe.config.{ Config, ConfigFactory }
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration._
import scala.language.postfixOps

object Main extends App with CrudResouces with LazyLogging {

  val config: Config = ConfigFactory.load()

  val port: Int = config.getInt("crud.port")

  val host: String = config.getString("crud.host")

  implicit lazy val system: ActorSystem = ActorSystem("crud-example")

  implicit val materializer = ActorMaterializer()

  implicit val executionContext = system.dispatcher

  implicit val timeout = Timeout(10 seconds)

  val api = routes

  Http().bindAndHandle(handler = api, interface = host, port = port) map { binding =>
    logger.info("REST interface bound to {}", binding.localAddress)
  } recover {
    case ex => logger.error(s"REST interface could not bind to $host:$port", ex.getMessage)
  }
}