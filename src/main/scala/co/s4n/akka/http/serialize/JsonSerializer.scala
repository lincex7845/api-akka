package co.s4n.akka.http.serialize

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import co.s4n.akka.http.dto.{ User, UserResponse }
import spray.json.{ DefaultJsonProtocol, PrettyPrinter }

trait JsonSerializer extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val printer = PrettyPrinter
  implicit val userFormat = jsonFormat2(User)
  implicit val userResponseFormat = jsonFormat2(UserResponse)
}