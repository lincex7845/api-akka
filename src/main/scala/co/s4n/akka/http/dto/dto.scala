package co.s4n.akka.http.dto

case class User(id: Int, name: String)

case class UserResponse(codigo: Int, mensaje: String)