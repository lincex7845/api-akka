package co.s4n.akka.http.route

import akka.http.scaladsl.server.{ Directives, Route }
import co.s4n.akka.http.dao.UserRepository
import co.s4n.akka.http.dto.{ User, UserResponse }
import co.s4n.akka.http.serialize.JsonSerializer

import scala.concurrent.ExecutionContext
import scala.util.{ Failure, Success }

trait CrudResouces extends Directives with JsonSerializer {

  implicit def executionContext: ExecutionContext

  lazy val usersRepo = UserRepository

  val routes: Route = pathPrefix("users") {
    get {
      pathSingleSlash {
        onComplete(usersRepo.getAll) {
          case Success(users) => complete(users)
          case Failure(t) => complete(builResponse(500, t.getMessage))
        }
      } ~
        path("get" / IntNumber) { id =>
          onComplete(usersRepo.getById(id)) {
            case Success(user) =>
              if (user.isDefined) complete(user)
              else complete(builResponse(404, s"The user $id is not registered yet"))
            case Failure(t) => complete(builResponse(500, t.getMessage))
          }
        }
    } ~
      post {
        path("create") {
          entity(as[User]) { user =>
            onComplete(usersRepo.save(user)) {
              case Success(_) => complete(builResponse(201, "The user was registered"))
              case Failure(t) => complete(builResponse(500, t.getMessage))
            }
          }
        }
      } ~
      put {
        path("update") {
          entity(as[User]) { user =>
            onComplete(usersRepo.save(user)) {
              case Success(_) => complete(builResponse(200, "The user was updated"))
              case Failure(t) => complete(builResponse(500, t.getMessage))
            }
          }
        }
      } ~
      delete {
        path("delete" / IntNumber) { id =>
          onComplete(usersRepo.delete(id)) {
            case Success(result) =>
              if (result) complete(builResponse(200, "The user was deleted"))
              else complete(builResponse(404, s"The user $id is not registered yet"))
            case Failure(t) => complete(builResponse(500, t.getMessage))
          }
        }
      }
  }

  private def builResponse(code: Int, message: String): UserResponse = UserResponse(code, message)
}
