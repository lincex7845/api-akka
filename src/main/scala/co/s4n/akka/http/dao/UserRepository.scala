package co.s4n.akka.http.dao

import co.s4n.akka.http.dto.User
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable.ListBuffer
import scala.concurrent.Future

trait UserRepository extends Repository[User, Int] {

  lazy val users = ListBuffer.empty[User]

  def getAll: Future[List[User]] = Future(users.toList)

  def getById(id: Int): Future[Option[User]] = Future(users.find(_.id.equals(id)))

  def save(user: User): Future[User] = {
    getById(user.id).map { oldUser =>
      if (oldUser.isDefined) {
        oldUser.map(user => remove(user))
      }
      users += user
      user
    }
  }

  def delete(id: Int): Future[Boolean] = {
    getById(id).map { userExists =>
      if (userExists.isDefined) {
        userExists.map(u => remove(u))
        true
      } else false
    }
  }

  private def remove(user: User): User = users.remove(users.indexOf(user))
}
object UserRepository extends UserRepository