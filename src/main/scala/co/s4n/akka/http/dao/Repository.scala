package co.s4n.akka.http.dao

import scala.concurrent.Future

trait Repository[T, idType] {

  def getAll: Future[List[T]]

  def getById(id: idType): Future[Option[T]]

  def save(entity: T): Future[T]

  def delete(id: idType): Future[Boolean]
}