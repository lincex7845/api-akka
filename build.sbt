scalariformSettings

organization := "co.s4n"

name := "akka.http.example"

scalaVersion := "2.12.1"

resolvers ++= Seq(
  "releases" at "http://oss.sonatype.org/content/repositories/releases"
)

libraryDependencies ++= Seq(
  "com.iheart"                  %%  "ficus"                     % "1.4.0",
  "ch.qos.logback"              %   "logback-classic"           % "1.1.7",
  "com.typesafe.scala-logging"  %%  "scala-logging"             % "3.5.0",
  "com.typesafe.akka"           %%  "akka-http"                 % "10.0.4" withSources() withJavadoc(), 
  "com.typesafe.akka"           %%  "akka-http-spray-json"      % "10.0.4" withSources() withJavadoc(),
  "org.scalatest"               %   "scalatest_2.12"            % "3.0.0"     % "test"
)

coverageEnabled := true

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xlint",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard",
  "-Xfuture",
  "-Xcheckinit"
)

publishMavenStyle := true

pomIncludeRepository := { _ => false }

publishArtifact in Test := false

publishTo := {
  val nexus = "http://somewhere/nexus/"
  if (version.value.trim.endsWith("SNAPSHOT"))
    Some("Nexus Snapshots" at nexus + "content/repositories/snapshots/")
  else
    Some("Nexus Releases" at nexus + "content/repositories/releases")
}

credentials += Credentials("Sonatype Nexus Repository Manager", "somewhere", "user", "password")
